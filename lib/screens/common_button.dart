import 'package:flutter/material.dart';

class AllButtons extends StatelessWidget {
  final double? height;
  final double? width;
  final String? fontfamily;
  final double? size;
  final Color? decorationColor;
  final Color? textColor;
  final BorderRadius? borderRadius;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final Color borderColor = Colors.blue;
  final String? lableText;

  AllButtons(
      {this.height,
      this.width,
      this.borderRadius,
      this.decorationColor,
      this.fontStyle,
      this.fontWeight,
      this.fontfamily,
      this.size,
      this.lableText,
      this.textColor});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.5, left: 10, right: 10),
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(color: decorationColor, borderRadius: borderRadius),
        child: Center(
          child: Text(
            lableText!,
            style: TextStyle(
              fontFamily: fontfamily,
              color: textColor,
              fontSize: size,
              fontWeight: fontWeight,
              fontStyle: fontStyle,
            ),
          ),
        ),
      ),
    );
  }
}
