import 'package:flutter/material.dart';

class AllText extends StatelessWidget {
  final String? textString;
  final String? textFamily;
  final FontStyle? textStyle;
  final double? textSize;
  final FontWeight? textWeight;
  final Color? textColor;

  AllText({
    this.textString,
    this.textFamily,
    this.textSize,
    this.textStyle,
    this.textWeight,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      textString!,
      style: TextStyle(
          fontFamily: textFamily,
          color: textColor,
          fontStyle: textStyle,
          fontSize: textSize,
          fontWeight: textWeight),
    );
  }
}
