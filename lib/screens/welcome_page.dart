import 'package:flutter/material.dart';
import 'package:login_page/screens/login_page.dart';

class WelcomePage extends StatefulWidget {
  String? email1;
  String? password1;

  WelcomePage({this.email1, this.password1});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  TextEditingController _email2 = TextEditingController();

  // @override
  // void initState() {
  //   _email2.text = widget.email1 ?? '';
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Welcome Page'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 180),
              Container(
                height: 200,
                color: Colors.grey,
                child: Padding(
                  padding: const EdgeInsets.only(top: 70),
                  child: Column(
                    children: [
                      Center(
                        child: Text(
                          'Email: ${args.email}',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Center(
                        child: Text(
                          'Password: ${args.password}',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.red,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              // Padding(
              //   padding: const EdgeInsets.all(15),
              //   child: TextFormField(
              //     controller: _email2,
              //     validator: (value) {
              //       if (value == null || value.isEmpty) {
              //         return 'Please enter email id';
              //       }
              //       if (value.contains('@')) {
              //         return null;
              //       } else {
              //         return 'Not valid email';
              //       }
              //     },
              //     textInputAction: TextInputAction.go,
              //     style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              //     obscureText: false,
              //     keyboardType: TextInputType.emailAddress,
              //     decoration: InputDecoration(
              //       labelText: "Email",
              //       labelStyle: TextStyle(
              //         fontSize: 16,
              //         fontWeight: FontWeight.w400,
              //         fontFamily: "SegoeUI",
              //         fontStyle: FontStyle.normal,
              //       ),
              //     ),
              //   ),
              // ),
              SizedBox(height: 30),
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(
                      context,
                      _email2.text,
                    );
                  },
                  child: Text('Go Back'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
