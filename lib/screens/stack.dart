import 'package:flutter/material.dart';
import 'package:login_page/screens/common_text.dart';

class MainImage extends StatelessWidget {
  const MainImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => Stack(
        children: [
          Container(
            height: 350,
            width: constraints.maxWidth,
            child: FittedBox(
              fit: BoxFit.cover,
              child: Image.asset(
                'assets/images/login_in_logo.png',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12, left: 12.5),
            child: AllText(
              textString: "Welcome",
              textFamily: "SegoeUI",
              textSize: 26,
              textColor: Colors.black,
              textStyle: FontStyle.normal,
              textWeight: FontWeight.w700,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40, left: 12.5),
            child: AllText(
              textString: "Sign in to continue",
              textFamily: "SegoeUI",
              textSize: 14,
              textColor: Color(0xffc7c7c7),
              textStyle: FontStyle.normal,
              textWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
