import 'package:flutter/material.dart';
import 'package:login_page/screens/common_button.dart';
import 'package:login_page/screens/common_text.dart';
import 'package:login_page/screens/stack.dart';
import 'package:login_page/screens/welcome_page.dart';

class LoginPageScreen extends StatefulWidget {
  @override
  _LoginPageScreenState createState() => _LoginPageScreenState();
}

class _LoginPageScreenState extends State<LoginPageScreen> {
  bool _isObscure = true;
  TextEditingController _emaill = TextEditingController();
  TextEditingController _passwordd = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        reverse: true,
        child: Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SafeArea(
            child: Column(
              children: [
                MainImage(),
                Padding(
                  padding: const EdgeInsets.only(top: 5, left: 15, right: 15),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: _emaill,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter email id';
                            }
                            if (value.contains('@')) {
                              return null;
                            } else {
                              return 'Not valid email';
                            }
                          },
                          textInputAction: TextInputAction.go,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                          obscureText: false,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            labelText: "Email",
                            labelStyle: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SegoeUI",
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                        TextFormField(
                          controller: _passwordd,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter password ';
                            }
                            if (value.length < 8) {
                              return "Please enter number more then 7";
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.go,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                          obscureText: _isObscure,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelText: "Password",
                            suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    _isObscure = !_isObscure;
                                  });
                                },
                                icon: Icon(_isObscure
                                    ? Icons.visibility
                                    : Icons.visibility_off)),
                            labelStyle: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SegoeUI",
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(
                      () {
                        print('Sign in button tapped');

                        if (formKey.currentState!.validate()) {}
                      },
                    );
                  },
                  child: AllButtons(
                    height: 50,
                    width: 370,
                    decorationColor: Colors.blue,
                    borderRadius: BorderRadius.circular(27),
                    lableText: "Sign In",
                    fontfamily: 'SegoeUI',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600,
                    size: 17,
                    textColor: Colors.white,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 08, left: 200),
                  child: InkWell(
                    onTap: () {
                      print('Forgot button Tapped!');
                    },
                    child: AllText(
                      textString: "Forgot Password?",
                      textFamily: "SegoeUI",
                      textColor: Colors.black38,
                      textSize: 15,
                      textWeight: FontWeight.w600,
                      textStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 08),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 85,
                        height: 2.0,
                        decoration: new BoxDecoration(
                          color: Color(0xffc7c7c7),
                        ),
                      ),
                      SizedBox(width: 15),
                      AllText(
                        textString: "OR",
                        textFamily: 'SegoeUI',
                        textColor: Color(0xff2e3755),
                        textSize: 17,
                        textWeight: FontWeight.w600,
                        textStyle: FontStyle.normal,
                      ),
                      SizedBox(width: 15),
                      Container(
                        width: 85,
                        height: 2.0,
                        decoration: new BoxDecoration(
                          color: Color(0xffc7c7c7),
                        ),
                      ),
                    ],
                  ),
                ),
                // SignInWithGoogle(),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: InkWell(
                    onTap: () async {
                      // var data = await Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) => WelcomePage(
                      //       email: _email.text,
                      //       password: _password.text,
                      //     ),
                      //   ),
                      // );

                      // _email.text = data;
                      Navigator.pushNamed(
                        context,
                        "welcomepage",
                        arguments: ScreenArguments(
                            '${_emaill.text}', '${_passwordd.text}'),
                      );
                    },
                    child: Container(
                      height: 50,
                      width: 370,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue, width: 2),
                          borderRadius: BorderRadius.circular(27)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/google_icon.png',
                            height: 30,
                          ),
                          SizedBox(width: 10),
                          Text(
                            'Sign in with Google',
                            style: TextStyle(
                              fontFamily: 'SegoeUI',
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ScreenArguments {
  final String email;
  final String password;
  ScreenArguments(this.email, this.password);
}
