import 'package:flutter/material.dart';

import 'package:login_page/screens/welcome_page.dart';
import 'screens/login_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login page EX',
      initialRoute: "login",
      routes: {
        "login": (context) => LoginPageScreen(),
        "welcomepage": (context) => WelcomePage(),
      },
      // home: LoginPageScreen(),
    );
  }
}
